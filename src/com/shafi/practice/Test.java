package com.shafi.practice;

public class Test {
    public static void main(String[] args) {
        int principal = (int) Console.readNumber("Principal: ", 1000.0, 1_000_000);
        float annualInterest = (float) Console.readNumber("Annual Interest Rate: ", 1, 30);
        short years = (short) Console.readNumber("Period (Years): ", 1, 30);

        MortgageReport.printMortgage(principal, annualInterest, years);


    }

}
