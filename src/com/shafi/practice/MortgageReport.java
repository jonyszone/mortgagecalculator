package com.shafi.practice;

import java.text.NumberFormat;

public class MortgageReport {

    public static void printMortgage(int principal, float annualInterest, short years) {
        System.out.println();

        double mortGage = Calculation.calculateMortgage(principal, annualInterest, years);

        String mortGageCurrency = NumberFormat.getCurrencyInstance().format(mortGage);
        System.out.println("Mortgage: " + mortGageCurrency);
    }
}
