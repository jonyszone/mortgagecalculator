package com.shafi.practice;

public class Calculation {

    public static double calculateMortgage(int principal, float annualInterest, short years) {

        final byte MONTHS_IN_YEAR = 12;
        final byte PERCENT = 100;

        float interestRate = annualInterest / PERCENT / MONTHS_IN_YEAR;
        short numberOfPayment = (short) (years * MONTHS_IN_YEAR);

        double mortGage = principal * (interestRate * Math.pow(1 + interestRate, numberOfPayment))
                / (Math.pow(1 + interestRate, numberOfPayment) - 1);
        return mortGage;

    }
}
