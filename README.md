# MortgageCalculator
## How to calculate mortgage payments
Want to figure out how much your monthly mortgage payment will be? For the mathematically inclined, here’s a formula to help you calculate mortgage payments manually:

# Equation for mortgage payments
-  M = P[r(1+r)^n/((1+r)^n)-1)]

-  M = the total monthly mortgage payment
-  P = the principal loan amount
-  r = your monthly interest rate. Lenders provide you an annual rate so you’ll need to divide that figure by 12 (the number of months in a year) to get the monthly rate. If your interest rate is 5%, your monthly rate would be 0.004167 (0.05/12=0.004167).
-  n = number of payments over the loan’s lifetime. Multiply the number of years in your loan term by 12 (the number of months in a year) to get the number of total payments for your loan. For example, a 30-year fixed mortgage would have 360 payments (30x12=360).

## Information From

[bankrate.com] (https://www.bankrate.com/calculators/mortgages/mortgage-calculator.aspx)